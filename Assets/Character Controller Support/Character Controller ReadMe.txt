=============================================
Jump Physics Character Controller

email:   poemdexter@gmail.com
twitter: @poemdexter
website: http://poemdexter.com/
=============================================

= Description = 

For those who imported the Character Controller package from the base Unity3D 
install, Jump Physics now supports the First Person Controller!

= Information = 

1. Download the JumpPhysicsController script from: 
	
	http://poemdexter.com/misc/JumpPhysicsController.zip

2. Unzip the script and place inside your project.
3. Import the Character Controller package from Assets > Import Package > 
   Character Controller
4. Place the First Person Controller into your Scene or Hierarchy view.
5. Attach the JumpPhysicsController.cs script to the First Person Controller.
6. In The First Person Controller, make the following changes:

- In the Movement section, set Gravity to 0.2
- In the Jumping section, set the Base Height and Extra Amount to 0 

And of course, if you have any questions feel free to email me or leave a 
comment on the asset store page. Enjoy!